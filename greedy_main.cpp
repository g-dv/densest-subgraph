/**
 * @author G-DV
 * @created dec. 2018
 * @description Greedy algorithm to find a potential
 *              densest subgraph efficiently.
 *              Works on undirected, unweighted graphs with no loops
 */

#include <iostream>
#include <utility>
#include <vector>
#include <fstream>
#include <algorithm>
#include <ctime>
#include <string>
#include <iomanip>
#include <unordered_map>
#include <limits>
#include <cstdlib>

#define MAX_N_NODES 100000

using namespace std;

typedef unsigned int uint;
typedef pair<uint, uint> Edge;
typedef vector<uint> Node;

struct Graph
{
    uint nEdges = 0;
    uint nNodes = 0;
    uint maxDegree = 0;
    uint minDegree = numeric_limits<uint>::max();
    vector<unordered_map<uint, Node>> neighbors;
    // Example usage: To get the neighbors of a node :
    //   neighbors[DEGREE_OF_THE_NODE][NODE_NUMBER]
    uint degrees[MAX_N_NODES];
    // Example usage: To get the degree of a node :
    //   degrees[NODE_NUMBER]
};

Graph readFile(const string &path, Graph &G)
{
    // initialize the map of degrees
    // degrees[i] gives the degree of the node number i
    for (int i = 0; i < MAX_N_NODES; i++)
        G.degrees[i] = 0;
    // actually read the file
    ifstream file(path);
    uint nodes[2];
    vector<Edge> edges;
    while (file >> nodes[0] >> nodes[1])
    {
        if (nodes[0] == nodes[1])
        {
            cout << "This kind of graph is supported: "
                 << "presence of a loop on node " << nodes[0] << endl;
            cout << "Exiting..." << endl;
            exit(EXIT_FAILURE);
        }
        // update our counters
        for (int i = 0; i < 2; i++)
        {
            // have we already met this node?
            if (!G.degrees[nodes[i]])
                G.nNodes++;
            // incement the degree of the node
            G.degrees[nodes[i]]++;
        }
        // add the edge to the temporary list of edges this
        // list will be used to populate the graph as soon
        // as we have the missing info (degrees, etc.)
        Edge edge(nodes[0], nodes[1]);
        edges.push_back(edge);
    }
    file.close();

    // now we need to finish building the graph

    // set the nEdges attribute
    G.nEdges = edges.size();

    // set the min/maxDegree attributes
    for (int i = 1; i <= G.nNodes; i++)
    {
        if (G.degrees[i] > G.maxDegree)
            G.maxDegree = G.degrees[i];
        if (G.degrees[i] < G.minDegree)
            G.minDegree = G.degrees[i];
    }

    // initalize the vector now that we know the max degree
    G.neighbors = vector<unordered_map<uint, Node>>(G.maxDegree + 1);

    // sort the nodes depending on their degree
    for (int i = 1; i <= G.nNodes; i++)
    {
        G.neighbors[G.degrees[i]].insert(make_pair(i, Node()));
    }

    // set the neighbors for each node
    for (Edge edge : edges)
    {
        G.neighbors[G.degrees[edge.first]][edge.first].push_back(edge.second);
        G.neighbors[G.degrees[edge.second]][edge.second].push_back(edge.first);
    }
    return G;
}

uint findMinDegreeNode(Graph &graph)
{
    // simply return any node that sorted among the min degree-nodes
    return graph.neighbors[graph.minDegree].begin()->first;
}

double getDensity(Graph &graph)
{
    // avoid floating point exception
    if (!graph.nNodes)
        return 0;
    // |E| / |V|
    return ((double)graph.nEdges / graph.nNodes);
}

void removeNode(uint node, Graph &graph, uint degree)
{
    // update the number of edges (substract the edges linked to the node)
    graph.nEdges -= graph.neighbors[degree][node].size();

    // update the neighbors
    for (uint neighbor : graph.neighbors[degree][node])
    {
        // decrement the degree of the neighbor
        graph.degrees[neighbor]--;
        uint neighborDegree = graph.degrees[neighbor];
        // the neighbor shouldn't be linked to the node we are removing anymore
        // use the erase-remove trick
        Node *tmp = &(graph.neighbors[neighborDegree + 1][neighbor]);
        tmp->erase(remove(tmp->begin(), tmp->end(), node), tmp->end());
        // since we decremented the degree of the neighbor, we must move it:
        // - add the neighbor to the right group
        graph.neighbors[neighborDegree].insert(make_pair(neighbor, *tmp));
        // - remove the neighbor to the previous group
        graph.neighbors[neighborDegree + 1].erase(neighbor);
        // we may need to update the value now that we downgraded the neighbor
        if (neighborDegree < graph.minDegree)
            graph.minDegree = neighborDegree;
    }

    // remove the node
    graph.neighbors[degree].erase(node);

    // since we removed a node, we may need to increase the min degree:
    // if the node was the last one with this degree, and if no neighbor
    // updated the min degree, then we must increase the min degree until
    // we reach more nodes
    if (!graph.neighbors[degree].size() && degree == graph.minDegree)
    {
        while (!graph.neighbors[++graph.minDegree].size())
            ;
    }

    // decrement the number of nodes
    graph.nNodes--;
}

int main(int argc, char *argv[])
{

    for (int i = 1; i <= 11; i++)
    {
        Graph G, H;

        readFile("./graphs/test_" + to_string(i) + ".txt", G);

        cout << G.nEdges << " " << G.nNodes << " ";

        clock_t startTime = clock();
        // pretty straight forward: we apply the algorithm seen in class
        while (G.nEdges)
        {
            uint node = findMinDegreeNode(G);
            removeNode(node, G, G.minDegree);
            if (getDensity(G) > getDensity(H))
            {
                // instead of doing "H = G" we copy only the
                // important attributes for optimization
                H.nEdges = G.nEdges;
                H.nNodes = G.nNodes;
            }
        }
        clock_t endTime = clock();
        float timeEllapsed = 1000.0 * (endTime - startTime) / CLOCKS_PER_SEC; // in ms

        cout << fixed << setprecision(2) << timeEllapsed << " "
             << getDensity(H) << " " << H.nNodes << endl;
    }

    return 0;
}
