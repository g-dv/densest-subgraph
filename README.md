# Densest Subgraph

Implémentation et comparaison de deux algorithmes pour déterminer le sous-graphe le plus dense.

## 1. Contexte

J'ai réalisé ce petit projet dans le cadre d'un cours de théorie des graphes. L'objectif est de trouver une approximation du sous-graphe le plus dense en utilisant deux algorithmes qui peuvent fonctionner sur des graphes extrêmement volumineux.

Les applications sont multiples ; on peut par exemple se servir de ces algorithmes pour détecter des communautés sur les réseaux sociaux ou encore parmi les auteurs d'articles scientifiques via leurs références.

## 2. Algorithme vorace

### 2.1. Méthode

Nous avons implémenté en C++ le pseudo-code suivant :
```
H = G
while (G contains at least one edge)
    let v be the node with minimum degree in G
    remove v and all its edges from G
    if density(G) > density(H) then
        H = G
return H
```

La difficulté de l’exercice est qu’une implémentation naïve requiert un temps d’exécution polynomial. Pour éviter le plus possible cela, nous avons :

- Utilisé une table pour accéder et modifier en temps constant le degré d’un noeud. Il s’agit d’un tableau dans lequel le degré du noeud i est stocké à l’indice ​i.

- Utilisé une variable `minDegree` : le degré du noeud ayant le degré le plus faible. Elle est mise à jour à chaque fois qu’un noeud change de degré et qu’un noeud est retiré.

- Classé les noeuds dans des groupes en fonction de leur degré. En effet, on veut pouvoir trouver le noeud de degré le plus faible en temps constant. Pour réussir cela, on a stocké les noeuds dans des groupes correspondant à leur degré, comme ça il suffit de choisir n’importe quel noeud situé dans le groupe pointé par la variable `minDegree`.

- Fait attention quand on supprime un noeud à :
    - mettre à jour le degré des voisins en temps constant grâce à la table
    - déplacer les voisins dans le groupe correspondant à leur nouveau degré, et ce en complexité très faible grâce à notre structure de données
    - mettre à jour la variable `minDegree` quand nécessaire

- Remplacé l’instruction `H ← G` par simplement la copie des attributs qui nous intéressent (nombre de noeuds et nombre d’arcs). En effet, une copie complète des noeuds et des arcs s’effectue en temps linéaire. Donc il est inconcevable d’effectuer cette instruction à chaque fois que le retrait d’un noeud améliore le résultat.

- Préféré l’utilisation des arrays ou de `unordered_map` plutôt que de `​map` parce que :
    - l’accès aux éléments de `​map` se fait en moyenne en temps logarithmique, ce qui dans notre cas va nous empêcher d’être en temps linéaire
    - l’accès aux éléments d’un array se fait en temps constant
    - l’accès aux éléments de `unordered_map` se fait en moyenne en temps constant (mais en pire cas en temps linéaire quand il y a trop de collisions)

Finalement, on a utilisé Python pour automatiquement dessiner sur un graphique les points (​nombre de noeuds ; temps requis).

### 2.2. Résultats

| G                                                         | \|E\|(G) | \|V\|(G) | µs    | ρ(H)  | \|V\|(H) |
| --------------------------------------------------------- | -------- | -------- | ----- | ----- | -------- |
| [Link](http://konect.cc/networks/ucidata-zachary/)        | 78       | 34       | 20    | 2.62  | 13       |
| [Link](http://konect.cc/networks/dimacs10-football/)      | 613      | 115      | 120   | 5.32  | 114      |
| [Link](http://konect.cc/networks/gene_fusion/)            | 279      | 291      | 70    | 1.75  | 12       |
| [Link](http://konect.cc/networks/subelj_euroroad/)        | 1417     | 1174     | 310   | 1.45  | 394      |
| [Link](http://konect.cc/networks/dimacs10-netscience/)    | 2742     | 1461     | 510   | 9.50  | 20       |
| [Link](http://konect.cc/networks/opsahl-powergrid/)       | 6594     | 4941     | 1580  | 03.08 | 13       |
| [Link](http://konect.cc/networks/pajek-erdos/)            | 11850    | 6927     | 4780  | 7.78  | 65       |
| [Link](http://konect.cc/networks/twin/)                   | 20573    | 14274    | 6570  | 13.00 | 27       |
| [Link](http://konect.cc/networks/dimacs10-astro-ph/)      | 121251   | 16046    | 30940 | 28.74 | 170      |
| [Link](http://konect.cc/networks/dimacs10-cond-mat-2003/) | 120029   | 30460    | 35410 | 12.78 | 27       |
| [Link](http://konect.cc/networks/dimacs10-cond-mat-2005/) | 175691   | 39577    | 57740 | 15.39 | 603      |

![Performances de l'algorithme vorace](images/vorace.png)

### 2.3. Autocritique

Les résultats sont satisfaisants et la vitesse d’exécution est très rapide, mais notre programme a tout de même des défauts :

- Notre programme traite un type de graphe particulier. Il n’est pas apte à traiter des graphes dirigés, pondérés ou ayant des noeuds pointant vers eux-mêmes. Il faudrait apporter des modifications au programme pour permettre cela.

- Le temps d’exécution n’est pas parfaitement linéaire et proportionnel au nombre de noeuds parce que :
    - Même si ce n’est pas dit en théorie, en pratique, on s’aperçoit qu’il y a souvent une corrélation entre le nombre de noeuds dans un graphe et le degré des noeuds. Donc, la méthode `removeNode` s'effectue souvent plus lentement quand le nombre de noeuds est plus important.
    - La méthode `removeNode` fait des accès aux éléments de la `unordered_map`,​ or en pire cas, sa complexité n’est pas constante. Donc le temps d’exécution de `removeNode` ​ est linéaire en pire cas.

## 3. Algorithme de streaming

Cet algorithme cherche à minimiser l'utilisation de la mémoire.

### 3.1. Méthode

Cette fois encore, l’implémentation naïve mène à un temps d’exécution polynomial, ce que nous voulons éviter. Pour optimiser le temps d’exécution, nous avons notamment :

- Commencé par traverser une première fois le fichier pour compter le nombre de noeuds. Cela nous permet d’allouer une seule fois la mémoire et donc d’éviter de coûteux redimensionnements. (Ajouter un élément à la fin d’un vecteur se fait en temps logarithmique car un dépassement de capacité entraîne en arrière-plan une copie des éléments dans un espace mémoire plus grand.)

- Alloué deux tableaux de `nNoeuds` éléments pour déterminer en temps constant si un noeud est encore présent dans le graphe ou non, et si oui, quel est son degré.

- Traversé une deuxième fois le fichier pour définir les degrés de chaque sommet. (complexité O(nArcs)) ​

- Lancé la boucle (complexité O(​log(nNoeuds))), mais nous avons fait attention à recalculer le degré des sommets après avoir éliminé tous les noeuds, sans quoi on aurait eu une complexité polynomiale.

Cette méthode nous a permis d’avoir un algorithme très performant ! Notre complexité en temps est donc de O(nlogm) avec n le nombre d’arcs et m le nombre de noeuds. C’est ce qui était prévu par la théorie. En termes de mémoire, nous n’utilisons avec cette méthode que deux tableaux de taille égale au nombre de noeuds.

### 3.2. Résultats

#### 3.2.1. Complexité

Une analyse de ces résultats révèle une complexité dans l’ordre de nlogn, ce qui correspond à la théorie ! En effet, on espérait que la complexité soit de l’ordre de |E|log|V| car la théorie prouve qu’on fait log(|V|) passes qui prennent chacune dans l’ordre de |E| opérations.

| G                                                         | \|E\|(G) | \|V\|(G) | µs     | ρ(H)    | \|V\|(H) |
| --------------------------------------------------------- | -------- | -------- | ------ | ------- | -------- |
| [Link](http://konect.cc/networks/ucidata-zachary/)        | 78       | 34       | 120    | 2.29412 | 34       |
| [Link](http://konect.cc/networks/dimacs10-football/)      | 613      | 115      | 600    | 5.33043 | 115      |
| [Link](http://konect.cc/networks/gene_fusion/)            | 279      | 291      | 330    | 1.66667 | 9        |
| [Link](http://konect.cc/networks/subelj_euroroad/)        | 1417     | 1174     | 4350   | 1.20698 | 1174     |
| [Link](http://konect.cc/networks/dimacs10-netscience/)    | 2742     | 1461     | 3850   | 9.04762 | 21       |
| [Link](http://konect.cc/networks/opsahl-powergrid/)       | 6594     | 4941     | 13390  | 2.76471 | 34       |
| [Link](http://konect.cc/networks/pajek-erdos/)            | 11850    | 6927     | 18670  | 7.14815 | 54       |
| [Link](http://konect.cc/networks/twin/)                   | 20573    | 14274    | 34220  | 13      | 27       |
| [Link](http://konect.cc/networks/dimacs10-astro-ph/)      | 121251   | 16046    | 166260 | 27.9474 | 114      |
| [Link](http://konect.cc/networks/dimacs10-cond-mat-2003/) | 120029   | 30460    | 243050 | 11.92   | 25       |
| [Link](http://konect.cc/networks/dimacs10-cond-mat-2005/) | 175691   | 39577    | 286940 | 14.1445 | 775      |


![Performances de l'algorithme de streaming](images/streaming.png)

#### 3.2.2. Impact du epsilon

Les deux courbes rouges et magenta semblent être décroissantes. Cela vérifie bien qu’un epsilon plus grand réduit généralement légèrement le temps d’exécution, mais que ce gain de temps s’effectue au prix de résultats généralement un peu moins précis.

| Epsilon | \|E\|(G) | \|V\|(G) | µs      | ρ(H)    | \|V\|(H) |
| ------- | -------- | -------- | ------- | ------- | -------- |
| 0.1     | 175691   | 39577    | 2621.73 | 14.1445 | 775      |
| 0.2     | 175691   | 39577    | 2836.31 | 13.5685 | 496      |
| 0.4     | 175691   | 39577    | 2381.74 | 12.7337 | 1288     |
| 0.6     | 175691   | 39577    | 2095.64 | 12.9436 | 887      |
| 0.8     | 175691   | 39577    | 2025.64 | 12.5864 | 631      |

![Impact de epsilon](images/epsilon.png)


#### 3.2.3. Mémoire réellement utilisée

Finalement, on vérifie avec l’outil valgrind la quantité de mémoire utilisée par le programme. On constate que le graphe qui pèse 1,9MB a pu être traité avec seulement 315kB, ce qui nous confirme que notre implémentation n’est pas trop gourmande en mémoire.

## 4. Comparaison et conclusion

| **Vorace**                                                                                                                                              | **Streaming**                                                                                      |
| ------------------------------------------------------------------------------------------------------------------------------------------------------- | -------------------------------------------------------------------------------------------------- |
| **Meilleure précision.** Les sous-graphes H trouvés ont une densité plus élevée                                                                         | **Moins bonne précision.** Les H trouvés ont généralement une densité plus faible                  |
| **Meilleur temps d’exécution.** Le temps d’exécution était un peu meilleur.                                                                           | **Moins bon temps d’exécution.** Mais le temps d’exécution  n’était pas beaucoup plus long.     |
| **Moins bonne complexité spatiale.** Valgrind nous a permis de vérifier que ce programme utilise plus de mémoire que n’en utilise le graphe d’entrée. | **Excellente complexité spatiale.** Le programme utilise moins de mémoire que le graphe d’entrée. |
