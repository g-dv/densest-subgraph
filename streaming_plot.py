import matplotlib.pyplot as plot
import sys

path = sys.argv[1]
try:
    file = open(path, "r")
except:
    print("Error: Could not open " + path)
plot.figure("Part 2 : Time efficiency")
x = []
y = []
lines = file.readlines()
for i in range(0, 11):
    line = lines[i]
    nEdgesG, nNodesG, time, densityH, nNodesH = [
        float(x) for x in line.split()]
    x.append(nNodesG)
    y.append(time)
plot.plot(x, y, 'go')
plot.title("Efficiency of the algorithm")
plot.xlabel('|V|')
plot.ylabel('Time (ms)')
plot.show()

x = [0.2, 0.4, 0.6, 0.8, 0.1]
y1 = []
y2 = []
for i in range(11, 16):
    line = lines[i]
    nEdgesG, nNodesG, time, densityH, nNodesH = [
        float(x) for x in line.split()]
    y1.append(densityH)
    y2.append(time)

plot.figure("Part 2 : Epsilon performance")

plot.subplot(2, 1, 1)
plot.plot(x, y1, 'ro')
plot.title("Epsilon: Comparison of results")
plot.ylabel('Density of H')

plot.subplot(2, 1, 2)
plot.plot(x, y2, 'mo')
plot.ylabel('Time')
plot.xlabel('epsilon')
plot.show()
