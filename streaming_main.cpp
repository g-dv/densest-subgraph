/**
 * @author G-DV
 * @created dec. 2018
 * @description Streaming algorithm to find a potential
 *              densest subgraph efficiently.
 *              Works on undirected, unweighted graphs
 */

#include <iostream>
#include <utility>
#include <string>
#include <vector>
#include <fstream>
#include <algorithm>
#include <ctime>

using namespace std;

typedef unsigned int uint;
typedef pair<uint, uint> Edge;

class MetaData
{
  public:
    uint nEdges = 0;
    uint nNodes = 0;
    double getDensity()
    {
        // avoid floating point exception
        if (!nNodes)
            return 0;
        // |E| / |V|
        return ((double)nEdges / nNodes);
    };
};

void countNodes(MetaData &metaData, const string &path)
{
    ifstream file(path);
    uint nodes[2];
    while (file >> nodes[0] >> nodes[1])
    {
        // update the total number of nodes
        for (int i = 0; i < 2; i++)
            if (metaData.nNodes < nodes[i])
                metaData.nNodes = nodes[i];
    }
    file.close();
}

void setDegrees(vector<uint> &degrees, const vector<bool> &selectionMask, MetaData &metaData, const string &path)
{
    fill(degrees.begin(), degrees.end(), 0);
    ifstream file(path);
    uint nodes[2];
    // we must also update the number of edges
    metaData.nEdges = 0;
    while (file >> nodes[0] >> nodes[1])
    {
        // are both the nodes are still in the graph?
        if (selectionMask[nodes[0]] && selectionMask[nodes[1]])
        {
            // this edge is in use!
            for (int i = 0; i < 2; i++)
                degrees[nodes[i]]++;
            metaData.nEdges++;
        }
    }
    file.close();
}

MetaData runAlgorithm(double parameter, const string &path)
{
    MetaData G, H;
    // we count the nodes so we can allocate directly enough memory
    // in order to avoid exceeding vector capacity
    countNodes(G, path);
    // initalize our data structures
    vector<uint> degrees(G.nNodes + 1, 0);          // degree of a node
    vector<bool> selectionMask(G.nNodes + 1, true); // true if a node is in the graph
    // we pass though the graph to find the degrees of the nodes
    setDegrees(degrees, selectionMask, G, path);
    cout << G.nEdges << " " << G.nNodes << " ";
    H = G;
    while (G.nEdges)
    {
        // calculate the floor under which we remove the nodes
        double degreeFloor = G.getDensity() * parameter;
        // loop through the nodes
        for (int i = 1; i < degrees.size(); i++)
        {
            // should we remove the node?
            if (selectionMask[i] && degrees[i] < degreeFloor)
            {
                // yes!
                selectionMask[i] = false;
                G.nNodes--;
            }
        }
        // we pass though the graph to recalculate the degrees of the nodes
        setDegrees(degrees, selectionMask, G, path);
        // is it better now?
        if (G.getDensity() > H.getDensity())
            H = G;
    }
    return H;
}

int main(int argc, char *argv[])
{
    // compare time depending on input size
    for (int i = 1; i <= 11; i++)
    {
        clock_t startTime = clock();
        string inputPath = "./graphs/test_" + to_string(i) + ".txt";

        const double EPSILON = 0.1;
        MetaData H = runAlgorithm(2 * (EPSILON + 1), inputPath);

        clock_t endTime = clock();
        float timeEllapsed = 1000.0 * (endTime - startTime) / CLOCKS_PER_SEC; // in ms

        cout << timeEllapsed << " " << H.getDensity() << " " << H.nNodes << endl;
    }
    // compare results depending on epsilon
    const double EPSILONS[5] = {0.2, 0.4, 0.6, 0.8, 0.1};
    for (int i = 0; i < 5; i++)
    {
        clock_t startTime = clock();
        string inputPath = "./graphs/test_8.txt"; // we use a 2MB file
        MetaData H = runAlgorithm(2 * (EPSILONS[i] + 1), inputPath);

        clock_t endTime = clock();
        float timeEllapsed = 1000.0 * (endTime - startTime) / CLOCKS_PER_SEC; // in ms

        cout << timeEllapsed << " " << H.getDensity() << " " << H.nNodes << endl;
    }

    return 0;
}
