import matplotlib.pyplot as plot
import sys

path = sys.argv[1]
try:
    file = open(path, "r")
except:
    print("Error: Could not open " + path)
plot.figure("Part 1 : Results")
x = []
y = []
for line in file:
    nEdgesG, nNodesG, time, densityH, nVerticesH = [
        float(x) for x in line.split()]
    x.append(nNodesG)
    y.append(time)
plot.plot(x, y, 'bo')
plot.title("Efficiency of the algorithm")
plot.xlabel('|V|')
plot.ylabel('Time (ms)')
plot.show()
