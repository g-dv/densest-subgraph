echo "compiling c++ script..."
g++ greedy_main.cpp -o greedy_main -O3
echo "running c++ script..."
./greedy_main > ./outputs/greedy_output.txt
echo "cleaning c++ binary..."
rm greedy_main
echo "running python script..."
python3 greedy_plot.py ./outputs/greedy_output.txt