echo "compiling c++ script..."
g++ streaming_main.cpp -o streaming_main -O3
echo "running c++ script..."
./streaming_main > ./outputs/streaming_output.txt
echo "cleaning c++ binary..."
rm streaming_main
echo "running python script..."
python3 streaming_plot.py ./outputs/streaming_output.txt